﻿Imports MySql.Data.MySqlClient
Public Class Connection
    Private conStr As String
    Private conDB As MySqlConnection
    Private conDB1 As MySqlConnection
    Public Shared MySetting As Boolean
    Private Sub Button_test_Click(sender As Object, e As EventArgs) Handles Button_test.Click
        ComboBox_db.Items.Clear()
        Try
            Me.conStr = "server=" & serverTxt.Text & ";user=" & userTxt.Text & ";password=" & passTxt.Text & ";database=information_schema;Convert Zero Datetime=True;Character Set=utf8;port=" & portTxt.Text & ";"
            '  Me.conStr = "server=" & My.Settings.Server & ";user=" & My.Settings.User & ";port=" & My.Settings.Port & ";password=" & My.Settings.Key & ";database=information_schema;Convert Zero Datetime=True;Character Set=utf8"
            conDB1 = New MySqlConnection(conStr)
            conDB1.Open()
            Dim sql1 As String = "select SCHEMA_NAME FROM SCHEMATA WHERE SCHEMA_NAME LIKE '%ptt_%' ORDER BY SCHEMA_NAME ASC"

            Dim query1 = New MySqlCommand(sql1, conDB1)
            Dim fetchArray1 As MySqlDataReader = query1.ExecuteReader()
            Do While fetchArray1.Read
                ComboBox_db.Items.Add(fetchArray1("SCHEMA_NAME"))
            Loop
            Label_status.Text = "Connect Success"
            Label_status.ForeColor = Color.Green
            connectBtn.Enabled = True

        Catch ex As Exception

            Label_status.Text = "Connect fail !!"
            Label_status.ForeColor = Color.Red
        Finally
            conDB1.Close()
        End Try
    End Sub

    Private Sub Connection_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        connectBtn.Enabled = False
        Label_status.Text = "Status Connection"
        Label_status.ForeColor = Color.Black
        Dim isconnect = False
        If My.Settings.Server = "" And My.Settings.User = "" And My.Settings.Key = "" And My.Settings.Port = "" Then
            My.Settings.Server = "127.0.0.1"
            My.Settings.User = "root"
            My.Settings.Key = "12345678"
            My.Settings.Port = "3306"
        End If

        If My.Settings.Port = "" Then
            portTxt.Text = "3306"
            My.Settings.Port = "3306"
        End If

        serverTxt.Text = My.Settings.Server
        userTxt.Text = My.Settings.User
        passTxt.Text = My.Settings.Key
        portTxt.Text = My.Settings.Port

        Me.conStr = "server=" & My.Settings.Server & ";user=" & My.Settings.User & ";port=" & My.Settings.Port & ";password=" & My.Settings.Key & ";database=" & My.Settings.DB_Name & ";Convert Zero Datetime=True;Character Set=utf8"
        conDB = New MySqlConnection(conStr)

        Try
            'ถ้่าต่อเข้า isconnect = true
            conDB.Open()
            If (My.Settings.DB_Name <> "") Then
                isconnect = True
            Else
                isconnect = False
            End If

        Catch myerror As MySqlException
            'ถ้าต่อไม่เข้า ขึ้น alert แล้ว isconnect = false
            MsgBox("Can't connect to Database, Please try again.")
            isconnect = False
        Finally
            conDB.Close()
        End Try

        'ดูค่า isconnect ว่าเป็น true หรือ false
        If isconnect Then

            If Not MySetting Then '<---- เช็คค่าตัวแปร mysetting (ค่าถูกเซ็ตเป็น false ในหน้า title, เป็น true จากหน้า frmSetting)

                'ตั้งค่าต่อ database และจำค่าไว้
                AppConfig.conStr = "server=" & My.Settings.Server & ";user=" & My.Settings.User & ";password=" & My.Settings.Key & ";port=" & My.Settings.Port & ";database=" & My.Settings.DB_Name & ";Convert Zero Datetime=True;Character Set=utf8;"
                AppConfig.conDB = New MySqlConnection(AppConfig.conStr)
                Me.DialogResult = Windows.Forms.DialogResult.Abort
                Me.Close()

                'ปิด form เพราะต่อเข้า database สำเร็จ
            Else

                'ล้างค่าต่อ database 
                serverTxt.Text = ""
                userTxt.Text = ""
                passTxt.Text = ""
                portTxt.Text = ""

            End If
        End If
    End Sub

    Private Sub connectBtn_Click(sender As Object, e As EventArgs) Handles connectBtn.Click
        If serverTxt.Text = "" Then
            MsgBox("Please enter server IP.")
            Exit Sub
        End If

        'ถ้ายังไม่ได้กรอก username ให้เด้งเตือน
        If userTxt.Text = "" Then
            MsgBox("Please enter server user.")
            Exit Sub
        End If

        If portTxt.Text = "" Then
            MsgBox("Please enter port.")
            Exit Sub
        End If

        If ComboBox_db.Text <> "" Then
            Dim isconnect = False
            Me.conStr = "server=" & serverTxt.Text & ";user=" & userTxt.Text & ";password=" & passTxt.Text & ";database=" & ComboBox_db.Text.Trim() & ";Convert Zero Datetime=True;Character Set=utf8;port=" & portTxt.Text & ";"
            conDB = New MySqlConnection(conStr)

            Try
                'ต่อสำเร็จ ให้ค่า isconnect = true
                conDB.Open()
                Me.DialogResult = Windows.Forms.DialogResult.OK
                isconnect = True
                My.Settings.DB_Name = ComboBox_db.Text
            Catch myerror As MySqlException

                'ต่อไม่สำเร็จ ให้ค่า isconnect = false
                MsgBox(myerror.ToString)
                isconnect = False
            End Try

            'ดูค่า isconnect ว่าเป็น true หรือ false
            If isconnect Then

                'ต่อสำเร็จ นำค่าการเชื่อมต่อบันทึกในตัวแปร my setting
                conDB.Close()

                My.Settings.Server = serverTxt.Text
                My.Settings.User = userTxt.Text
                My.Settings.Key = passTxt.Text
                My.Settings.Port = portTxt.Text
                Me.DialogResult = Windows.Forms.DialogResult.OK


                AppConfig.conStr = "server=" & My.Settings.Server & ";user=" & My.Settings.User & ";password=" & My.Settings.Key & ";port=" & My.Settings.Port & ";database=" & My.Settings.DB_Name & ";Convert Zero Datetime=True;Character Set=utf8;"
                AppConfig.conDB = New MySqlConnection(AppConfig.conStr)
                Me.Close() 'ปิดฟอร์ม
            End If
        Else
            MessageBox.Show("Please select DB_Name")
        End If
    End Sub

    Private Sub cancelBtn_Click(sender As Object, e As EventArgs) Handles cancelBtn.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
End Class
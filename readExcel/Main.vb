﻿Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Excel
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.IO
Imports MySql.Data.MySqlClient

Public Class Main
    Dim id_bank As String
    Dim name_bank As String
    Dim id_acc As String
    Dim acc_no As String
    Dim acc_name As String
    Dim bookmax
    Dim _cultureEnInfo As New Globalization.CultureInfo("en-US")
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_start.Click
        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As Excel.Worksheet
        Dim range As Excel.Range
        Dim rCnt As Integer
        Dim cCnt As Integer
        Dim Obj As Object

        xlApp = New Excel.Application
        xlWorkBook = xlApp.Workbooks.Open(TextBox_file.Text)
        xlWorkSheet = xlWorkBook.Worksheets("sheet1")
        'display the colum , row

        range = xlWorkSheet.UsedRange

        For rCnt = 1 To range.Rows.Count
            Dim c, p, a, d, m As String
            For cCnt = 1 To range.Columns.Count
                Obj = CType(range.Cells(rCnt, cCnt), Excel.Range)
                If rCnt = 1 And cCnt = 1 Then
                    acc_no = Obj.value
                    bookmax = insert_book()
                    If bookmax = 0 Then
                        Exit Sub
                    End If
                End If
                If rCnt > 5 Then
                    ' MsgBox(Obj.value)
                    If cCnt = 1 Then
                        c = Obj.value
                    End If
                    If cCnt = 2 Then
                        p = Obj.value
                    End If
                    If cCnt = 3 Then
                        a = Obj.value
                    End If
                    If cCnt = 4 Then
                        d = Obj.value
                    End If
                    If cCnt = 5 Then
                        m = Obj.value
                    End If

                End If
            Next
            If c <> String.Empty Then
                insert_chq(bookmax, c, p, a, d, m)
            End If

        Next
        MessageBox.Show("SucessFull")
        xlWorkBook.Close()
        xlApp.Quit()

        releaseObject(xlApp)
        releaseObject(xlWorkBook)
        releaseObject(xlWorkSheet)

    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub Button_browse_Click(sender As Object, e As EventArgs) Handles Button_browse.Click
        Dim dlg As New OpenFileDialog()
        If dlg.ShowDialog() = DialogResult.OK Then
            TextBox_file.Text = dlg.FileName
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Connection.MySetting = False '<--- เช็ตค่าตัวแปร mysetting ใน frmConnectSetting เป็น false
        Connection.ShowDialog()  '<--- เรียกฟอร์ม frmConnectSetting
        If Connection.DialogResult = DialogResult.Cancel Then
            Environment.Exit(0)
            Exit Sub
        End If
    End Sub

    Private Sub Button_cs_Click(sender As Object, e As EventArgs) Handles Button_cs.Click
        Connection.MySetting = True
        Connection.ShowDialog()
        If Connection.DialogResult = DialogResult.OK Then
            MsgBox("Please restart program to change connection setting")
            Environment.Exit(0)
        End If
    End Sub
    Private Function insert_book()

        Dim sql1 As String = "select id,acc_no,acc_name,bank_id from lc_account where acc_no='" & acc_no & "'"
        AppConfig.ConnectDB()
        Dim query1 = New MySqlCommand(sql1, AppConfig.conDB)
        Dim fetchArray1 As MySqlDataReader = query1.ExecuteReader()
        If Not fetchArray1.HasRows Then
            MessageBox.Show("Account not found.")
            AppConfig.CloseDB()
            Return 0
        Else
            Do While fetchArray1.Read
                id_acc = fetchArray1.GetString("id")
                acc_no = fetchArray1.GetString("acc_no")
                acc_name = fetchArray1.GetString("acc_name")
                id_bank = fetchArray1.GetString("bank_id")
            Loop
            AppConfig.CloseDB()
        End If

        Dim sql2 As String = "select short_name from lc_bank where id='" & id_bank & "'"
        AppConfig.ConnectDB()
        Dim query2 = New MySqlCommand(sql2, AppConfig.conDB)
        Dim fetchArray2 As MySqlDataReader = query2.ExecuteReader()
        Do While fetchArray2.Read
            name_bank = fetchArray2.GetString("short_name")
        Loop
        AppConfig.CloseDB()


        AppConfig.ConnectDB()
        Dim max = "SELECT MAX(book_no)+1 as bookmax FROM lc_book"
        Dim query = New MySqlCommand(max, AppConfig.conDB)
        Dim fetchArray As MySqlDataReader = query.ExecuteReader()
        If Not fetchArray.HasRows Then
            max = 1
        Else
            Do While fetchArray.Read
                Try
                    max = fetchArray.GetString("bookmax")
                Catch ex As Exception
                    max = 1
                End Try
            Loop
        End If
        AppConfig.CloseDB()

        AppConfig.ConnectDB()
        Dim in_book = "insert INTO `lc_book` (`id`, `book_no`, `account`, `create`) VALUES ('id'," + max + ", " + id_acc + ",'" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", _cultureEnInfo) + "')"
        Dim cmd = New MySqlCommand(in_book, AppConfig.conDB)
        cmd.ExecuteNonQuery()
        AppConfig.CloseDB()
        Return max
    End Function

    Private Sub insert_chq(book_max As String, chq As String, pay As String, amount As String, printdate As Date, memo As String)
        '   MessageBox.Show(book_max, chq + pay + amount + printdate + memo)
        AppConfig.ConnectDB()
        Dim Sql = "INSERT INTO lc_cheque(id, book,chq_no,pay1,pay2,paystub1,paystub2,paystub3,amount,amount_txt_l1,amount_txt_l2,print_date,ac_payee,or_bearer,memo,memostub,logo,ac_pic,logo_pic,status,approve,approve_date) VALUES (id," 'id
        Sql &= book_max & "," ' book
        Sql &= "'" & chq & "'," 'chq_no
        Sql &= "'" & pay & "'," 'pay1
        Sql &= "''," 'pay2
        Sql &= "''," 'paystub1
        Sql &= "''," 'paystub2
        Sql &= "''," 'paystub3
        Sql &= "'" & amount & "'," 'amount
        Sql &= "'" & MoneyCV.NumberToWords(CDbl(amount)) & "'," 'atext line 1
        Sql &= "''," 'atext line 2
        Sql &= "'" & printdate.ToString("yyyy-MM-dd", _cultureEnInfo) & "'," 'print_date
        Sql &= "'0'," 'ac_payee
        Sql &= "'0'," 'or_bearer
        Sql &= "''," 'memo
        Sql &= "''," 'memo_stub
        Sql &= "'0'," 'logo
        Sql &= "''," 'ac_pic
        Sql &= "''," 'logo_pic
        Sql &= "'2'," 'status
        Sql &= "'0'," 'approve
        Sql &= "'0000-00-00');" 'approve Date
        Sql &= " SELECT LAST_INSERT_ID()"
        Dim query = New MySqlCommand(Sql, AppConfig.conDB)
        Dim returnID As Integer = CInt(query.ExecuteScalar())
        AppConfig.CloseDB()

        AppConfig.ConnectDB()
        Dim Sql3 = "INSERT INTO report_balancesheet(id, bank_id,bank_name,acc_id,acc_no,acc_name,date,chq_no,item_desc,received,payment,recv_id,chq_id) VALUES (id," 'id
        Sql3 &= id_bank & "," ' bank_id
        Sql3 &= "'" & name_bank & "'," 'bank_name
        Sql3 &= "'" & id_acc & "'," 'acc_id
        Sql3 &= "'" & acc_no & "'," 'acc_no
        Sql3 &= "'" & acc_name & "'," 'acc_name
        Sql3 &= "'" & printdate.ToString("yyyy-MM-dd", _cultureEnInfo) & "'," 'date
        Sql3 &= "'" & chq & "'," 'chq_no
        Sql3 &= "''," 'item_desc
        Sql3 &= "'0'," 'received
        Sql3 &= "'" & amount & "'," 'payment
        Sql3 &= "'0'," 'recv_id 
        Sql3 &= "'" & returnID & "');" 'chq_id
        'Sql &= " SELECT LAST_INSERT_ID()"
        Dim query3 = New MySqlCommand(Sql3, AppConfig.conDB)
        query3.ExecuteScalar()
        AppConfig.CloseDB()
    End Sub
End Class

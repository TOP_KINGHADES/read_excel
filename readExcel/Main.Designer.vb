﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Button_start = New System.Windows.Forms.Button()
        Me.TextBox_file = New System.Windows.Forms.TextBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Button_browse = New System.Windows.Forms.Button()
        Me.Button_cs = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Button_start
        '
        Me.Button_start.Location = New System.Drawing.Point(319, 10)
        Me.Button_start.Name = "Button_start"
        Me.Button_start.Size = New System.Drawing.Size(75, 23)
        Me.Button_start.TabIndex = 0
        Me.Button_start.Text = "Start"
        Me.Button_start.UseVisualStyleBackColor = True
        '
        'TextBox_file
        '
        Me.TextBox_file.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextBox_file.Location = New System.Drawing.Point(12, 11)
        Me.TextBox_file.Name = "TextBox_file"
        Me.TextBox_file.Size = New System.Drawing.Size(218, 21)
        Me.TextBox_file.TabIndex = 1
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Button_browse
        '
        Me.Button_browse.Location = New System.Drawing.Point(238, 9)
        Me.Button_browse.Name = "Button_browse"
        Me.Button_browse.Size = New System.Drawing.Size(75, 23)
        Me.Button_browse.TabIndex = 2
        Me.Button_browse.Text = "Browse"
        Me.Button_browse.UseVisualStyleBackColor = True
        '
        'Button_cs
        '
        Me.Button_cs.Location = New System.Drawing.Point(400, 11)
        Me.Button_cs.Name = "Button_cs"
        Me.Button_cs.Size = New System.Drawing.Size(145, 23)
        Me.Button_cs.TabIndex = 3
        Me.Button_cs.Text = "Connection Setting"
        Me.Button_cs.UseVisualStyleBackColor = True
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(555, 44)
        Me.Controls.Add(Me.Button_cs)
        Me.Controls.Add(Me.Button_browse)
        Me.Controls.Add(Me.TextBox_file)
        Me.Controls.Add(Me.Button_start)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Transfer Data Excel"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button_start As System.Windows.Forms.Button
    Friend WithEvents TextBox_file As System.Windows.Forms.TextBox
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents Button_browse As Button
    Friend WithEvents Button_cs As Button
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Connection
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label_status = New System.Windows.Forms.Label()
        Me.Button_test = New System.Windows.Forms.Button()
        Me.ComboBox_db = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cancelBtn = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.portTxt = New System.Windows.Forms.TextBox()
        Me.passTxt = New System.Windows.Forms.TextBox()
        Me.userTxt = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.serverTxt = New System.Windows.Forms.TextBox()
        Me.connectBtn = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label_status
        '
        Me.Label_status.AutoSize = True
        Me.Label_status.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_status.Location = New System.Drawing.Point(200, 252)
        Me.Label_status.Name = "Label_status"
        Me.Label_status.Size = New System.Drawing.Size(141, 20)
        Me.Label_status.TabIndex = 30
        Me.Label_status.Text = "Status Connection"
        '
        'Button_test
        '
        Me.Button_test.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_test.Location = New System.Drawing.Point(152, 188)
        Me.Button_test.Name = "Button_test"
        Me.Button_test.Size = New System.Drawing.Size(227, 32)
        Me.Button_test.TabIndex = 29
        Me.Button_test.Text = "Test Connect"
        Me.Button_test.UseVisualStyleBackColor = True
        '
        'ComboBox_db
        '
        Me.ComboBox_db.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox_db.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox_db.FormattingEnabled = True
        Me.ComboBox_db.Location = New System.Drawing.Point(151, 289)
        Me.ComboBox_db.Name = "ComboBox_db"
        Me.ComboBox_db.Size = New System.Drawing.Size(234, 28)
        Me.ComboBox_db.TabIndex = 28
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(34, 290)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(87, 20)
        Me.Label5.TabIndex = 27
        Me.Label5.Text = "Database :"
        '
        'cancelBtn
        '
        Me.cancelBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cancelBtn.Location = New System.Drawing.Point(275, 328)
        Me.cancelBtn.Name = "cancelBtn"
        Me.cancelBtn.Size = New System.Drawing.Size(110, 32)
        Me.cancelBtn.TabIndex = 25
        Me.cancelBtn.Text = "Cancel"
        Me.cancelBtn.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(80, 149)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 20)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "Port :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(41, 109)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(86, 20)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "Password :"
        '
        'portTxt
        '
        Me.portTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.portTxt.Location = New System.Drawing.Point(152, 146)
        Me.portTxt.Name = "portTxt"
        Me.portTxt.Size = New System.Drawing.Size(227, 26)
        Me.portTxt.TabIndex = 22
        Me.portTxt.Text = "3306"
        Me.portTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'passTxt
        '
        Me.passTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.passTxt.Location = New System.Drawing.Point(152, 106)
        Me.passTxt.Name = "passTxt"
        Me.passTxt.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.passTxt.Size = New System.Drawing.Size(227, 26)
        Me.passTxt.TabIndex = 21
        Me.passTxt.Text = "ying007"
        Me.passTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.passTxt.UseSystemPasswordChar = True
        '
        'userTxt
        '
        Me.userTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.userTxt.Location = New System.Drawing.Point(152, 66)
        Me.userTxt.Name = "userTxt"
        Me.userTxt.Size = New System.Drawing.Size(227, 26)
        Me.userTxt.TabIndex = 20
        Me.userTxt.Text = "root"
        Me.userTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(36, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 20)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Username :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(64, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 20)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Server :"
        '
        'serverTxt
        '
        Me.serverTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.serverTxt.Location = New System.Drawing.Point(151, 26)
        Me.serverTxt.Name = "serverTxt"
        Me.serverTxt.Size = New System.Drawing.Size(228, 26)
        Me.serverTxt.TabIndex = 17
        Me.serverTxt.Text = "127.0.0.1"
        Me.serverTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'connectBtn
        '
        Me.connectBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.connectBtn.Location = New System.Drawing.Point(151, 328)
        Me.connectBtn.Name = "connectBtn"
        Me.connectBtn.Size = New System.Drawing.Size(110, 32)
        Me.connectBtn.TabIndex = 16
        Me.connectBtn.Text = "Connect"
        Me.connectBtn.UseVisualStyleBackColor = True
        '
        'Connection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(423, 386)
        Me.Controls.Add(Me.Label_status)
        Me.Controls.Add(Me.Button_test)
        Me.Controls.Add(Me.ComboBox_db)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cancelBtn)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.portTxt)
        Me.Controls.Add(Me.passTxt)
        Me.Controls.Add(Me.userTxt)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.serverTxt)
        Me.Controls.Add(Me.connectBtn)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "Connection"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Connection"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label_status As Label
    Friend WithEvents Button_test As Button
    Friend WithEvents ComboBox_db As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents cancelBtn As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents portTxt As TextBox
    Friend WithEvents passTxt As TextBox
    Friend WithEvents userTxt As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents serverTxt As TextBox
    Friend WithEvents connectBtn As Button
End Class

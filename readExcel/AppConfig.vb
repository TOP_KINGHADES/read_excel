﻿Imports System.Globalization
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports System.Net.NetworkInformation
Imports System.IO

Public Class AppConfig
    Public Shared conStr As String = "server=" & My.Settings.Server & ";user=" & My.Settings.User & ";password=" & My.Settings.Key & ";port=" & My.Settings.Port & ";database=" & My.Settings.DB_Name & ";Convert Zero Datetime=True;Character Set=utf8;"
    Public Shared conDB As New MySqlConnection(conStr)

    'เปิดการเชื่อมต่อ ดาต้าเบส
    Public Shared Sub ConnectDB()
        Try
            conDB.Open()
        Catch myerror As MySqlException
            MessageBox.Show("Cannot connect to database: " & myerror.Message)

            Application.Exit()
        End Try
    End Sub

    'ปิดการเชื่อมต่อดาต้าเบส
    Public Shared Sub CloseDB()
        conDB.Close()
    End Sub



End Class

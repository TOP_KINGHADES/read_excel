﻿Option Explicit On
Option Strict On

Public Class MoneyCV

    Shared z As Char = "0"c

    Public Shared Function NumberToWords(ByVal InputNumber As Double) As String
        'If My.Settings.Language = "en" Then
        '    NumberToWords = My.Settings.AWords_LeftSymbol & NumberToEngWords(InputNumber.ToString) & My.Settings.AWords_RightSymbol
        'ElseIf My.Settings.Language = "th" Then
        '    NumberToWords = My.Settings.AWords_LeftSymbol & NumberToThaiWord(InputNumber) & My.Settings.AWords_RightSymbol
        'End If

        '   If My.Settings.Language_word = "th" Then
        NumberToWords = My.Settings.AWords_LeftSymbol & NumberToThaiWord(InputNumber) & My.Settings.AWords_RightSymbol
        '  ElseIf My.Settings.Language_word = "lao" Then
        '    NumberToWords = My.Settings.AWords_LeftSymbol & NumberToLoaWord(InputNumber) & My.Settings.AWords_RightSymbol
        '    Else
        '   NumberToWords = My.Settings.AWords_LeftSymbol & NumberToEngWords(InputNumber.ToString) & My.Settings.AWords_RightSymbol
        '   End If
    End Function
    Public Shared Function NumberToLoaWordExam(ByVal InputNumber As Double, ByVal currency As String, ByVal cent As String, ByVal plural As String) As String
        If InputNumber = 0 Then
            NumberToLoaWordExam = "ສູນກີບ"
            Return NumberToLoaWordExam
        End If

        Dim NewInputNumber As String
        NewInputNumber = InputNumber.ToString("###0.00")

        If CDbl(NewInputNumber) >= 10000000000000 Then
            NumberToLoaWordExam = ""
            Return NumberToLoaWordExam
        End If

        Dim tmpNumber(2) As String
        Dim FirstNumber As String
        Dim LastNumber As String

        tmpNumber = NewInputNumber.Split(CChar("."))
        FirstNumber = tmpNumber(0)
        LastNumber = tmpNumber(1)

        Dim nLength As Integer = 0
        nLength = CInt(FirstNumber.Length)

        Dim i As Integer
        Dim CNumber As Integer = 0
        Dim CNumberBak As Integer = 0
        Dim strNumber As String = ""
        Dim strPosition As String = ""
        Dim FinalWord As String = ""
        Dim CountPos As Integer = 0

        For i = nLength To 1 Step -1
            CNumberBak = CNumber
            CNumber = CInt(FirstNumber.Substring(CountPos, 1))


            If CNumber = 0 AndAlso i = 7 Then
                strPosition = "ລ້ານ"
            ElseIf CNumber = 0 Then
                strPosition = ""
            Else
                strPosition = PositionToTextLoa(i)
            End If

            If CNumber = 2 AndAlso strPosition = "ສິບ" Then
                strNumber = "ຊາວ"
            ElseIf CNumber = 1 AndAlso strPosition = "ສິບ" Then
                strNumber = ""
            ElseIf CNumber = 1 AndAlso strPosition = "ລ້ານ" AndAlso nLength >= 8 Then
                If CNumberBak = 0 Then
                    strNumber = "ຫນຶ່ງ"
                Else
                    strNumber = "ເອັດ"
                End If
            ElseIf CNumber = 1 AndAlso strPosition = "" AndAlso nLength > 1 Then
                strNumber = "ເອັດ"
            Else
                strNumber = NumberToTextLoa(CNumber)
            End If

            CountPos = CountPos + 1
            FinalWord = FinalWord & strNumber & strPosition
        Next

        CountPos = 0
        CNumberBak = 0
        nLength = CInt(LastNumber.Length)

        Dim Stang As String = ""
        Dim FinalStang As String = ""

        If CDbl(LastNumber) > 0.0 Then
            For i = nLength To 1 Step -1
                CNumberBak = CNumber
                CNumber = CInt(LastNumber.Substring(CountPos, 1))

                If CNumber = 1 AndAlso i = 2 Then
                    strPosition = "ສິບ"
                ElseIf CNumber = 0 Then
                    strPosition = ""
                Else
                    strPosition = PositionToTextLoa(i)
                End If

                If CNumber = 2 AndAlso strPosition = "ສິບ" Then
                    Stang = "ຊາວ"
                    strPosition = ""
                ElseIf CNumber = 1 AndAlso i = 2 Then
                    Stang = ""
                ElseIf CNumber = 1 AndAlso strPosition = "" AndAlso nLength > 1 Then
                    If CNumberBak = 0 Then
                        Stang = "ຫນຶ່ງ"
                    Else
                        Stang = "ເອັດ"
                    End If
                Else
                    Stang = NumberToTextLoa(CNumber)
                End If

                CountPos = CountPos + 1

                FinalStang = FinalStang & Stang & strPosition
            Next

            FinalStang = FinalStang & cent
        Else
            FinalStang = ""
        End If

        Dim SubUnit As String
        If FinalStang = "" Then
            If currency = "บาท" Then
                SubUnit = "บาทถ้วน"
            Else
                SubUnit = currency
            End If
        Else
            If CDbl(FirstNumber) <> 0 Then
                SubUnit = currency
            Else
                SubUnit = ""
            End If
        End If
        NumberToLoaWordExam = FinalWord & SubUnit & FinalStang
        If (NumberToLoaWordExam.Contains("ຊາວສິບ")) Then
            NumberToLoaWordExam = NumberToLoaWordExam.Replace("ຊາວສິບ", "ຊາວ")
        ElseIf (NumberToLoaWordExam.Contains("ຫນຶ່ງຕື້")) Then
            NumberToLoaWordExam = NumberToLoaWordExam.Replace("ຫນຶ່ງຕື້", "ຕື້")
            If (NumberToLoaWordExam.Contains("ຕື້")) Then
                NumberToLoaWordExam = NumberToLoaWordExam.Replace("ຕື້ລ້ານ", "ຕື້")
            End If
        ElseIf (NumberToLoaWordExam.Contains("ຕື້")) Then
            NumberToLoaWordExam = NumberToLoaWordExam.Replace("ຕື້ລ້ານ", "ຕື້")
        End If
    End Function

    Private Shared Function PositionToTextLoa(ByVal CurrentPos As Integer) As String
        Dim _nPos As String = ""

        Select Case CurrentPos
            Case 0
                _nPos = ""
            Case 1
                _nPos = ""
            Case 2
                _nPos = "ສິບ"
            Case 3
                _nPos = "ຮ້ອຍ"
            Case 4
                _nPos = "ພັນ"
            Case 5
                _nPos = "ສິບພັນ"
            Case 6
                _nPos = "ແສນ"
            Case 7
                _nPos = "ລ້ານ"
            Case 8
                _nPos = "ສິບ"
            Case 9
                _nPos = "ຮ້ອຍ"
            Case 10
                _nPos = "ຕື້"
            Case 11
                _nPos = "ສິບຕື້"
            Case 12
                _nPos = "ຮ້ອຍຕື້"
            Case 13
                _nPos = "ພັນຕື້"
        End Select

        PositionToTextLoa = _nPos
    End Function

    Public Shared Function NumberToLoaWord(ByVal InputNumber As Double) As String
        If InputNumber = 0 Then
            NumberToLoaWord = "ສູນກີບ"
            Return NumberToLoaWord
        End If

        Dim NewInputNumber As String
        NewInputNumber = InputNumber.ToString("###.00")
        ' NewInputNumber = NewInputNumber.Replace(",", ".")
        If CDbl(NewInputNumber) >= 10000000000000 Then
            NumberToLoaWord = ""
            Return NumberToLoaWord
        End If

        Dim tmpNumber(2) As String
        Dim FirstNumber As String
        Dim LastNumber As String

        tmpNumber = NewInputNumber.Split(CChar("."))
        FirstNumber = tmpNumber(0)
        LastNumber = tmpNumber(1)

        Dim nLength As Integer = 0
        nLength = CInt(FirstNumber.Length)

        Dim i As Integer
        Dim CNumber As Integer = 0
        Dim CNumberBak As Integer = 0
        Dim strNumber As String = ""
        Dim strPosition As String = ""
        Dim oldposition As String = ""
        Dim FinalWord As String = ""
        Dim CountPos As Integer = 0

        For i = nLength To 1 Step -1
            CNumberBak = CNumber
            CNumber = CInt(FirstNumber.Substring(CountPos, 1))

            If CNumber = 0 AndAlso i = 7 Then
                strPosition = "ລ້ານ"
            ElseIf CNumber = 0 Then
                strPosition = ""
            Else
                strPosition = PositionToTextLoa(i)
            End If



            If CNumber = 2 AndAlso strPosition = "ສິບ" Then
                strNumber = "ຊາວ"
            ElseIf CNumber = 1 AndAlso strPosition = "ສິບ" Then
                strNumber = ""
            ElseIf CNumber = 1 AndAlso strPosition = "ລ້ານ" AndAlso nLength >= 8 Then
                If CNumberBak = 0 Then
                    strNumber = "ຫນຶ່ງ"
                Else
                    strNumber = "ເອັດ"
                End If
            ElseIf CNumber = 1 AndAlso strPosition = "" AndAlso nLength > 1 Then
                If oldposition = "ສິບ" Then
                    strNumber = "ເອັດ"
                Else
                    strNumber = "ຫນຶ່ງ"
                End If
            ElseIf CNumber = 1 AndAlso strPosition = "ພັນ" AndAlso nLength > 1 Then
                If oldposition = "ສິບພັນ" Then
                    strNumber = "ເອັດ"
                Else
                    strNumber = "ຫນຶ່ງ"
                End If
            Else
                strNumber = NumberToTextLoa(CNumber)
            End If
            If CNumber <> 0 AndAlso strPosition = "ພັນ" AndAlso nLength > 1 Then
                FinalWord = FinalWord.Replace("ສິບພັນ", "ສິບ")
            End If
            If CNumber <> 0 AndAlso strPosition = "ສິບຕື້" AndAlso nLength > 10 Then
                FinalWord = FinalWord.Replace("ສິບຕື້", "ສິບ")
            End If
            If CNumber <> 0 AndAlso strPosition = "ສິບຕື້" AndAlso nLength > 11 Then
                FinalWord = FinalWord.Replace("ຮ້ອຍຕື້", "ຮ້ອຍ")
            End If
            If CNumber <> 0 AndAlso strPosition = "ຮ້ອຍຕື້" AndAlso nLength > 12 Then
                FinalWord = FinalWord.Replace("ພັນຕື້", "ພັນ")
            End If

            oldposition = strPosition
            CountPos = CountPos + 1

            If (strNumber = "ຊາວ") Then
                FinalWord = FinalWord & strNumber
            Else
                FinalWord = FinalWord & strNumber & strPosition
            End If
        Next

        CountPos = 0
        CNumberBak = 0
        nLength = CInt(LastNumber.Length)

        Dim Stang As String = ""
        Dim FinalStang As String = ""

        If CDbl(LastNumber) > 0.0 Then
            For i = nLength To 1 Step -1
                CNumberBak = CNumber
                CNumber = CInt(LastNumber.Substring(CountPos, 1))

                If CNumber = 1 AndAlso i = 2 Then
                    strPosition = "ສິບ"
                ElseIf CNumber = 0 Then
                    strPosition = ""
                Else
                    strPosition = PositionToTextLoa(i)
                End If

                If CNumber = 2 AndAlso strPosition = "ສິບ" Then
                    Stang = "ຊາວ"
                    strPosition = ""
                ElseIf CNumber = 1 AndAlso i = 2 Then
                    Stang = ""
                ElseIf CNumber = 1 AndAlso strPosition = "" AndAlso nLength > 1 Then
                    If CNumberBak = 0 Then
                        Stang = "ຫນຶ່ງ"
                    Else
                        Stang = "ເອັດ"
                    End If
                Else
                    Stang = NumberToTextLoa(CNumber)
                End If

                CountPos = CountPos + 1
                FinalStang = FinalStang & Stang & strPosition
            Next

            FinalStang = My.Settings.Currency_Sub & FinalStang
        Else
            FinalStang = ""
        End If

        Dim SubUnit As String
        If FinalStang = "" Then
            If My.Settings.Currency_unit = "บาท" Then
                SubUnit = "บาทถ้วน"
            Else
                SubUnit = My.Settings.Currency_unit
            End If
        Else
            If CDbl(FirstNumber) <> 0 Then
                SubUnit = My.Settings.Currency_unit
            Else
                SubUnit = ""
            End If
        End If

        NumberToLoaWord = FinalWord & SubUnit & FinalStang

        If (NumberToLoaWord.Contains("ຫນຶ່ງຕື້ລ້ານ")) Then
            NumberToLoaWord = NumberToLoaWord.Replace("ຫນຶ່ງຕື້ລ້ານ", "ຫນຶ່ງຕື້")
        ElseIf (NumberToLoaWord.Contains("ສິບຕື້ຫນຶ່ງຕື້")) Then
            NumberToLoaWord = NumberToLoaWord.Replace("ສິບຕື້ຫນຶ່ງຕື້", "ສິບເອັດຕື້")
        ElseIf (NumberToLoaWord.Contains("ສອງສິບຕື້")) Then
            NumberToLoaWord = NumberToLoaWord.Replace("ສອງສິບຕື້", "ຊາວ")
        ElseIf (NumberToLoaWord.Contains("ສາມສິບຕື້")) Then
            NumberToLoaWord = NumberToLoaWord.Replace("ສາມສິບຕື້", "ສາມສິບ")
        ElseIf (NumberToLoaWord.Contains("ສີ່ສິບຕື້")) Then
            NumberToLoaWord = NumberToLoaWord.Replace("ສີ່ສິບຕື້", "ສີ່ສິບ")
        ElseIf (NumberToLoaWord.Contains("ຫ້າສິບຕື້")) Then
            NumberToLoaWord = NumberToLoaWord.Replace("ຫ້າສິບຕື້", "ຫ້າສິບ")
        ElseIf (NumberToLoaWord.Contains("ຫົກສິບຕື້")) Then
            NumberToLoaWord = NumberToLoaWord.Replace("ຫົກສິບຕື້", "ຫົກສິບ")
        ElseIf (NumberToLoaWord.Contains("ເຈັດສິບຕື້")) Then
            NumberToLoaWord = NumberToLoaWord.Replace("ເຈັດສິບຕື້", "ເຈັດສິບ")

        ElseIf (NumberToLoaWord.Contains("ແປດສິບຕື້")) Then
            NumberToLoaWord = NumberToLoaWord.Replace("ແປດສິບຕື້", "ແປດສິບ")

        ElseIf (NumberToLoaWord.Contains("ເກົ້າສິບຕື້")) Then
            NumberToLoaWord = NumberToLoaWord.Replace("ເກົ້າສິບຕື້", "ເກົ້າສິບ")

        ElseIf (NumberToLoaWord.Contains("ຮ້ອຍຕື້ລ້ານກີບ")) Then
            NumberToLoaWord = NumberToLoaWord.Replace("ຮ້ອຍຕື້ລ້ານກີບ", "ຮ້ອຍຕື້ກີບ")
        End If

        'If (NumberToLoaWord.Contains("ຫນຶ່ງສິບພັນຫນຶ່ງ")) Then
        '    NumberToLoaWord = NumberToLoaWord.Replace("ຫນຶ່ງສິບພັນຫນຶ່ງ", "ສິບເອັດ")

        If (NumberToLoaWord.Contains("ຫນຶ່ງສິບ")) Then
            NumberToLoaWord = NumberToLoaWord.Replace("ຫນຶ່ງສິບ", "ສິບ")

        ElseIf (NumberToLoaWord.Contains("ສອງສິບ")) Then
            NumberToLoaWord = NumberToLoaWord.Replace("ສອງສິບ", "ຊາວ")
        End If



        If (NumberToLoaWord.Contains("ພັນຕື້ລ້ານ")) Then
            NumberToLoaWord = NumberToLoaWord.Replace("ພັນຕື້ລ້ານ", "ພັນຕື້")
        End If

        If (NumberToLoaWord.Contains("ຕື້ສິບຕື້ຫນຶ່ງ")) Then
            NumberToLoaWord = NumberToLoaWord.Replace("ຕື້ສິບຕື້ຫນຶ່ງ", "ສິບເອັດ")
        End If

        If (NumberToLoaWord.Contains("ສິບຕື້ຫນຶ່ງ")) Then
            NumberToLoaWord = NumberToLoaWord.Replace("ສິບຕື້ຫນຶ່ງ", "ສິບເອັດ")
        End If
        'ElseIf (NumberToLoaWord.Contains("ສາມສິບພັນ")) Then
        '    NumberToLoaWord = NumberToLoaWord.Replace("ສາມສິບພັນ", "ສາມສິບ")

        'ElseIf (NumberToLoaWord.Contains("ສີ່ສິບພັນ")) Then
        '    NumberToLoaWord = NumberToLoaWord.Replace("ສີ່ສິບພັນ", "ສີ່ສິບ")

        'ElseIf (NumberToLoaWord.Contains("ຫ້າສິບພັນ")) Then
        '    NumberToLoaWord = NumberToLoaWord.Replace("ຫ້າສິບພັນ", "ຫ້າສິບ")

        'ElseIf (NumberToLoaWord.Contains("ຫົກສິບພັນ")) Then
        '    NumberToLoaWord = NumberToLoaWord.Replace("ຫົກສິບພັນ", "ຫົກສິບ")

        'ElseIf (NumberToLoaWord.Contains("ເຈັດສິບພັນ")) Then
        '    NumberToLoaWord = NumberToLoaWord.Replace("ເຈັດສິບພັນ", "ເຈັດສິບ")

        'ElseIf (NumberToLoaWord.Contains("ແປດສິບພັນ")) Then
        '    NumberToLoaWord = NumberToLoaWord.Replace("ແປດສິບພັນ", "ແປດສິບ")

        'ElseIf (NumberToLoaWord.Contains("ເກົ້າສິບພັນ")) Then
        '    NumberToLoaWord = NumberToLoaWord.Replace("ເກົ້າສິບພັນ", "ເກົ້າສິບ")
        'End If




    End Function

    Public Shared Function NumberToTextLoa(ByVal CurrentNum As Integer) As String
        Dim _nText As String = ""

        Select Case CurrentNum
            Case 0
                _nText = ""
            Case 1
                _nText = "ຫນຶ່ງ"
            Case 2
                _nText = "ສອງ"
            Case 3
                _nText = "ສາມ"
            Case 4
                _nText = "ສີ່"
            Case 5
                _nText = "ຫ້າ"
            Case 6
                _nText = "ຫົກ"
            Case 7
                _nText = "ເຈັດ"
            Case 8
                _nText = "ແປດ"
            Case 9
                _nText = "ເກົ້າ"
        End Select

        NumberToTextLoa = _nText
    End Function

    Public Shared Function NumberToThaiWord(ByVal InputNumber As Double) As String
        If InputNumber = 0 Then
            NumberToThaiWord = "ศูนย์บาทถ้วน"
            Return NumberToThaiWord
        End If

        Dim NewInputNumber As String
        NewInputNumber = InputNumber.ToString("###0.00")

        If CDbl(NewInputNumber) >= 10000000000000 Then
            NumberToThaiWord = ""
            Return NumberToThaiWord
        End If

        Dim tmpNumber(2) As String
        Dim FirstNumber As String
        Dim LastNumber As String

        tmpNumber = NewInputNumber.Split(CChar("."))
        FirstNumber = tmpNumber(0)
        LastNumber = tmpNumber(1)

        Dim nLength As Integer = 0
        nLength = CInt(FirstNumber.Length)

        Dim i As Integer
        Dim CNumber As Integer = 0
        Dim CNumberBak As Integer = 0
        Dim strNumber As String = ""
        Dim strPosition As String = ""
        Dim oldposition As String = ""
        Dim FinalWord As String = ""
        Dim CountPos As Integer = 0

        For i = nLength To 1 Step -1
            CNumberBak = CNumber
            CNumber = CInt(FirstNumber.Substring(CountPos, 1))

            If CNumber = 0 AndAlso i = 7 Then
                strPosition = "ล้าน"
            ElseIf CNumber = 0 Then
                strPosition = ""
            Else
                strPosition = PositionToText(i)
            End If

            If CNumber = 2 AndAlso strPosition = "สิบ" Then
                strNumber = "ยี่"
            ElseIf CNumber = 1 AndAlso strPosition = "สิบ" Then
                strNumber = ""
            ElseIf CNumber = 1 AndAlso strPosition = "ล้าน" AndAlso nLength >= 8 Then
                If CNumberBak = 0 Then
                    strNumber = "หนึ่ง"
                Else
                    strNumber = "เอ็ด"
                End If
            ElseIf CNumber = 1 AndAlso strPosition = "" AndAlso nLength > 1 Then
                If oldposition = "สิบ" Then
                    strNumber = "เอ็ด"
                Else
                    strNumber = "หนึ่ง"
                End If
            Else
                strNumber = NumberToText(CNumber)
            End If

            oldposition = strPosition
            CountPos = CountPos + 1

            FinalWord = FinalWord & strNumber & strPosition
        Next

        CountPos = 0
        CNumberBak = 0
        nLength = CInt(LastNumber.Length)

        Dim Stang As String = ""
        Dim FinalStang As String = ""

        If CDbl(LastNumber) > 0.0 Then
            For i = nLength To 1 Step -1
                CNumberBak = CNumber
                CNumber = CInt(LastNumber.Substring(CountPos, 1))

                If CNumber = 1 AndAlso i = 2 Then
                    strPosition = "สิบ"
                ElseIf CNumber = 0 Then
                    strPosition = ""
                Else
                    strPosition = PositionToText(i)
                End If

                If CNumber = 2 AndAlso strPosition = "สิบ" Then
                    Stang = "ยี่"
                ElseIf CNumber = 1 AndAlso i = 2 Then
                    Stang = ""
                ElseIf CNumber = 1 AndAlso strPosition = "" AndAlso nLength > 1 Then
                    If CNumberBak = 0 Then
                        Stang = "หนึ่ง"
                    Else
                        Stang = "เอ็ด"
                    End If
                Else
                    Stang = NumberToText(CNumber)
                End If

                CountPos = CountPos + 1

                FinalStang = FinalStang & Stang & strPosition
            Next

            FinalStang = FinalStang & My.Settings.Currency_Sub
        Else
            FinalStang = ""
        End If

        Dim SubUnit As String
        If FinalStang = "" Then
            If My.Settings.Currency_unit = "บาท" Then
                SubUnit = "บาทถ้วน"
            Else
                SubUnit = My.Settings.Currency_unit
            End If
        Else
            If CDbl(FirstNumber) <> 0 Then
                SubUnit = My.Settings.Currency_unit
            Else
                SubUnit = ""
            End If
        End If

        NumberToThaiWord = FinalWord & SubUnit & FinalStang
    End Function

    Private Shared Function NumberToText(ByVal CurrentNum As Integer) As String
        Dim _nText As String = ""

        Select Case CurrentNum
            Case 0
                _nText = ""
            Case 1
                _nText = "หนึ่ง"
            Case 2
                _nText = "สอง"
            Case 3
                _nText = "สาม"
            Case 4
                _nText = "สี่"
            Case 5
                _nText = "ห้า"
            Case 6
                _nText = "หก"
            Case 7
                _nText = "เจ็ด"
            Case 8
                _nText = "แปด"
            Case 9
                _nText = "เก้า"
        End Select

        NumberToText = _nText
    End Function

    Private Shared Function PositionToText(ByVal CurrentPos As Integer) As String
        Dim _nPos As String = ""

        Select Case CurrentPos
            Case 0
                _nPos = ""
            Case 1
                _nPos = ""
            Case 2
                _nPos = "สิบ"
            Case 3
                _nPos = "ร้อย"
            Case 4
                _nPos = "พัน"
            Case 5
                _nPos = "หมื่น"
            Case 6
                _nPos = "แสน"
            Case 7
                _nPos = "ล้าน"
            Case 8
                _nPos = "สิบ"
            Case 9
                _nPos = "ร้อย"
            Case 10
                _nPos = "พัน"
            Case 11
                _nPos = "หมื่น"
            Case 12
                _nPos = "แสน"
            Case 13
                _nPos = "ล้าน"
        End Select

        PositionToText = _nPos
    End Function

    'CONVERT TO DOLLA
    Public Shared Function NumberToEngWords(ByVal amount As String) As String

        Dim dollars, cents, temp As String
        Dim decimalPlace, count As Integer
        Dim place(9) As String
        place(2) = " Thousand "
        place(3) = " Million "
        place(4) = " Billion "
        place(5) = " Trillion "

        ' String representation of amount.
        amount = amount.Trim()
        amount = amount.Replace(",", "")
        ' Position of decimal place 0 if none.
        decimalPlace = amount.IndexOf(".")
        ' Convert cents and set string amount to dollar amount.
        If decimalPlace > 0 Then
            ' cents = GetTens(amount.Substring(decimalPlace + 1).PadRight(2, z).Substring(0, 2))
            cents = amount.Substring(decimalPlace + 1).PadRight(2, z).Substring(0, 2)
            amount = amount.Substring(0, decimalPlace).Trim()
        End If

        count = 1
        Do While amount <> ""
            temp = GetHundreds(amount.Substring(Math.Max(amount.Length, 3) - 3))
            If temp <> "" Then dollars = temp & place(count) & dollars
            If amount.Length > 3 Then
                amount = amount.Substring(0, amount.Length - 3)
            Else
                amount = ""
            End If
            count = count + 1
        Loop

        Select Case dollars
            Case ""
                dollars = "No " & My.Settings.Currency_unit
            Case "One"
                dollars = "One " & My.Settings.Currency_unit
            Case Else
                dollars = dollars & " " & My.Settings.Currency_unit
        End Select

        ' Select Case cents
        '     Case ""
        ' cents = " and No " & My.Settings.Currency_Sub & My.Settings.Currency_Plural
        '     Case "One"
        ' cents = " and One " & My.Settings.Currency_Sub
        '     Case Else
        ' cents = " and " & cents & " " & My.Settings.Currency_Sub & My.Settings.Currency_Plural
        ' End Select
        If cents = "" Then
            cents = "00"
        End If

        If My.Settings.centOnly Then
            If cents = "00" Then
                cents = " Only"
            Else
                Dim centsTemp = GetHundreds(cents)
                If cents.Substring(1) <> "0" Then
                    cents = " and " & centsTemp & " " & My.Settings.Currency_Sub
                Else
                    cents = " and " & centsTemp & My.Settings.Currency_Sub
                End If
                'cents = " and " & centsTemp & " " & My.Settings.Currency_Sub
            End If
        Else
            cents = " and " & cents & "/100"

        End If

        'cents = " and " & cents & "/100"

        Return dollars & cents
    End Function

    ' Converts a number from 100-999 into text
    Private Shared Function GetHundreds(ByVal amount As String) As String
        Dim Result As String
        If Not Integer.Parse(amount) = 0 Then
            amount = amount.PadLeft(3, z)
            ' Convert the hundreds place.
            If amount.Substring(0, 1) <> "0" Then
                Result = GetDigit(amount.Substring(0, 1)) & " Hundred "
            End If
            ' Convert the tens and ones place.
            If amount.Substring(1, 1) <> "0" Then
                Result = Result & GetTens(amount.Substring(1))
            Else
                Result = Result & GetDigit(amount.Substring(2))
            End If
            GetHundreds = Result
        End If
    End Function

    ' Converts a number from 10 to 99 into text.
    Private Shared Function GetTens(ByRef TensText As String) As String
        Dim Result As String
        Result = ""           ' Null out the temporary function value.
        If TensText.StartsWith("1") Then   ' If value between 10-19...
            Select Case Integer.Parse(TensText)
                Case 10 : Result = "Ten"
                Case 11 : Result = "Eleven"
                Case 12 : Result = "Twelve"
                Case 13 : Result = "Thirteen"
                Case 14 : Result = "Fourteen"
                Case 15 : Result = "Fifteen"
                Case 16 : Result = "Sixteen"
                Case 17 : Result = "Seventeen"
                Case 18 : Result = "Eighteen"
                Case 19 : Result = "Nineteen"
                Case Else
            End Select
        Else                                 ' If value between 20-99...
            Select Case Integer.Parse(TensText.Substring(0, 1))
                Case 2 : Result = "Twenty "
                Case 3 : Result = "Thirty "
                Case 4 : Result = "Forty "
                Case 5 : Result = "Fifty "
                Case 6 : Result = "Sixty "
                Case 7 : Result = "Seventy "
                Case 8 : Result = "Eighty "
                Case 9 : Result = "Ninety "
                Case Else
            End Select
            Result = Result & GetDigit(TensText.Substring(1, 1))  ' Retrieve ones place.
        End If
        GetTens = Result
    End Function

    ' Converts a number from 1 to 9 into text.
    Private Shared Function GetDigit(ByRef Digit As String) As String
        Select Case Integer.Parse(Digit)
            Case 1 : GetDigit = "One"
            Case 2 : GetDigit = "Two"
            Case 3 : GetDigit = "Three"
            Case 4 : GetDigit = "Four"
            Case 5 : GetDigit = "Five"
            Case 6 : GetDigit = "Six"
            Case 7 : GetDigit = "Seven"
            Case 8 : GetDigit = "Eight"
            Case 9 : GetDigit = "Nine"
            Case Else : GetDigit = ""
        End Select
    End Function

    'Sample-----------------------------------------------
    Public Shared Function NumberToThaiWordExam(ByVal InputNumber As Double, ByVal currency As String, ByVal cent As String, ByVal plural As String) As String
        If InputNumber = 0 Then
            NumberToThaiWordExam = "ศูนย์บาทถ้วน"
            Return NumberToThaiWordExam
        End If

        Dim NewInputNumber As String
        NewInputNumber = InputNumber.ToString("###0.00")

        If CDbl(NewInputNumber) >= 10000000000000 Then
            NumberToThaiWordExam = ""
            Return NumberToThaiWordExam
        End If

        Dim tmpNumber(2) As String
        Dim FirstNumber As String
        Dim LastNumber As String

        tmpNumber = NewInputNumber.Split(CChar("."))
        FirstNumber = tmpNumber(0)
        LastNumber = tmpNumber(1)

        Dim nLength As Integer = 0
        nLength = CInt(FirstNumber.Length)

        Dim i As Integer
        Dim CNumber As Integer = 0
        Dim CNumberBak As Integer = 0
        Dim strNumber As String = ""
        Dim strPosition As String = ""
        Dim FinalWord As String = ""
        Dim CountPos As Integer = 0

        For i = nLength To 1 Step -1
            CNumberBak = CNumber
            CNumber = CInt(FirstNumber.Substring(CountPos, 1))

            If CNumber = 0 AndAlso i = 7 Then
                strPosition = "ล้าน"
            ElseIf CNumber = 0 Then
                strPosition = ""
            Else
                strPosition = PositionToText(i)
            End If

            If CNumber = 2 AndAlso strPosition = "สิบ" Then
                strNumber = "ยี่"
            ElseIf CNumber = 1 AndAlso strPosition = "สิบ" Then
                strNumber = ""
            ElseIf CNumber = 1 AndAlso strPosition = "ล้าน" AndAlso nLength >= 8 Then
                If CNumberBak = 0 Then
                    strNumber = "หนึ่ง"
                Else
                    strNumber = "เอ็ด"
                End If
            ElseIf CNumber = 1 AndAlso strPosition = "" AndAlso nLength > 1 Then
                strNumber = "เอ็ด"
            Else
                strNumber = NumberToText(CNumber)
            End If

            CountPos = CountPos + 1

            FinalWord = FinalWord & strNumber & strPosition
        Next

        CountPos = 0
        CNumberBak = 0
        nLength = CInt(LastNumber.Length)

        Dim Stang As String = ""
        Dim FinalStang As String = ""

        If CDbl(LastNumber) > 0.0 Then
            For i = nLength To 1 Step -1
                CNumberBak = CNumber
                CNumber = CInt(LastNumber.Substring(CountPos, 1))

                If CNumber = 1 AndAlso i = 2 Then
                    strPosition = "สิบ"
                ElseIf CNumber = 0 Then
                    strPosition = ""
                Else
                    strPosition = PositionToText(i)
                End If

                If CNumber = 2 AndAlso strPosition = "สิบ" Then
                    Stang = "ยี่"
                ElseIf CNumber = 1 AndAlso i = 2 Then
                    Stang = ""
                ElseIf CNumber = 1 AndAlso strPosition = "" AndAlso nLength > 1 Then
                    If CNumberBak = 0 Then
                        Stang = "หนึ่ง"
                    Else
                        Stang = "เอ็ด"
                    End If
                Else
                    Stang = NumberToText(CNumber)
                End If

                CountPos = CountPos + 1

                FinalStang = FinalStang & Stang & strPosition
            Next

            FinalStang = FinalStang & cent
        Else
            FinalStang = ""
        End If

        Dim SubUnit As String
        If FinalStang = "" Then
            If currency = "บาท" Then
                SubUnit = "บาทถ้วน"
            Else
                SubUnit = currency
            End If
        Else
            If CDbl(FirstNumber) <> 0 Then
                SubUnit = currency
            Else
                SubUnit = ""
            End If
        End If

        NumberToThaiWordExam = FinalWord & SubUnit & FinalStang
    End Function

    Public Shared Function NumberToEngWordsExam(ByVal amount As String, ByVal dollar As String, ByVal mycent As String, ByVal plural As String, ByVal centWords As Boolean) As String

        Dim dollars, cents, temp As String
        Dim decimalPlace, count As Integer
        Dim place(9) As String
        place(2) = " Thousand "
        place(3) = " Million "
        place(4) = " Billion "
        place(5) = " Trillion "

        ' String representation of amount.
        amount = amount.Trim()
        amount = amount.Replace(",", "")
        ' Position of decimal place 0 if none.
        decimalPlace = amount.IndexOf(".")
        ' Convert cents and set string amount to dollar amount.
        If decimalPlace > 0 Then
            ' cents = GetTens(amount.Substring(decimalPlace + 1).PadRight(2, z).Substring(0, 2))
            cents = amount.Substring(decimalPlace + 1).PadRight(2, z).Substring(0, 2)
            amount = amount.Substring(0, decimalPlace).Trim()
        End If

        count = 1
        Do While amount <> ""
            temp = GetHundreds(amount.Substring(Math.Max(amount.Length, 3) - 3))
            If temp <> "" Then dollars = temp & place(count) & dollars
            If amount.Length > 3 Then
                amount = amount.Substring(0, amount.Length - 3)
            Else
                amount = ""
            End If
            count = count + 1
        Loop

        Select Case dollars
            Case ""
                dollars = "No " & dollar ' & plural
            Case "One"
                dollars = "One " & dollar
            Case Else
                dollars = dollars & " " & dollar ' & plural
        End Select

        '  Select Case cents
        '     Case ""
        ' cents = " and No " & cent & plural
        '      Case "One"
        '  cents = " and One " & cent
        '      Case Else
        '  cents = " and " & cents & " " & cent & plural
        ' End Select
        If cents = "" Then
            cents = "00"
        End If

        If centWords Then
            If cents = "00" Then
                cents = " Only"
            Else
                Dim centsTemp = GetHundreds(cents)
                If cents.Substring(1) <> "0" Then
                    cents = " and " & centsTemp & " " & mycent
                Else
                    cents = " and " & centsTemp & mycent
                End If
            End If
        Else
            cents = " and " & cents & "/100"
        End If
        ' cents = " and " & cents & "/100"

        Return dollars & cents
    End Function


End Class